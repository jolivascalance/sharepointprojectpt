﻿using System;
using System.ComponentModel;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration.Claims;

namespace SharePointProjectPT.VisualWebPartInfoLibrarySSO
{
    [ToolboxItemAttribute(false)]
    public partial class VisualWebPartInfoLibrarySSO : WebPart
    {
        // Uncomment the following SecurityPermission attribute only when doing Performance Profiling on a farm solution
        // using the Instrumentation method, and then remove the SecurityPermission attribute when the code is ready
        // for production. Because the SecurityPermission attribute bypasses the security check for callers of
        // your constructor, it's not recommended for production purposes.
        // [System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.Assert, UnmanagedCode = true)]
        public VisualWebPartInfoLibrarySSO()
        {
        }

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            InitializeControl();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string INFORMATION_LIBRARY_URL = SPContext.Current.Web.Site.Url;
            string USER_ACCOUNT_LIST_NAME = "UserAccountInformation";
            
            string userEmail = SPContext.Current.Web.CurrentUser.Email;
            using (SPSite site = new SPSite(INFORMATION_LIBRARY_URL, SPUserToken.SystemAccount))
            {
                using (SPWeb web = site.OpenWeb())
                {
                    
                    // Build a query.
                    SPQuery query = new SPQuery();

                    query.Query = string.Concat(
                                        "<Where>",
                                            "<Eq>",
                                                 "<FieldRef Name='EMail' />",
                                                 "<Value Type='Text'>",
                                                 userEmail,
                                                 "</Value>",
                                            "</Eq>",
                                        "</Where>");

                    query.ViewFields = string.Concat(
                                        "<FieldRef Name='Title' />",
                                        "<FieldRef Name='Password' />",
                                        "<FieldRef Name='First_x0020_Name' />",
                                        "<FieldRef Name='Last_x0020_Name' />",
                                        "<FieldRef Name='DisplayName' />",
                                        "<FieldRef Name='EMail' />",
                                        "<FieldRef Name='ActiveDate' />",
                                        "<FieldRef Name='ExpireDate' />",
                                        "<FieldRef Name='Company_x0020_ID' />",
                                        "<FieldRef Name='Distributor_x0020_ID' />",
                                        "<FieldRef Name='User_x0020_Type' />",
                                        "<FieldRef Name='PT_x0020_Warranty_x0020_Access' />",
                                        "<FieldRef Name='PT_x0020_Warranty_x0020_Admin' />",
                                        "<FieldRef Name='ISZAJ' />",
                                        "<FieldRef Name='PT_x0020_Media' />");
                                        //"<FieldRef Name='ServiceHotlineID' />");



                    query.ViewFieldsOnly = true; // Fetch only the data that we need.

                    // Get data from a list.
                    SPList list = web.Lists.TryGetList(USER_ACCOUNT_LIST_NAME);
                    
                    SPListItemCollection items = null;
                    if (list != null)
                    {
                        items = list.GetItems(query);
                        int itemsCount = items.Count;

                        if (items != null && items.Count == 1)
                        {
                            SPListItem item = items[0];
                            //addSSOID(item.ID, list);
                            
                            string Username = Convert.ToString(item["Title"]);
                            string Password = Convert.ToString(item["Password"]);
                            string DisplayName = Convert.ToString(item["DisplayName"]);
                            string UserType = Convert.ToString(item["User_x0020_Type"]);
                            string PTWarrantyAccess = Convert.ToString(item["PT_x0020_Warranty_x0020_Access"]);
                            string IszjAccess = Convert.ToString(item["ISZAJ"]);
                            string PTMediaAccess = Convert.ToString(item["PT_x0020_Media"]);
                            //string ServiceHotlineID = Convert.ToString(item["ServiceHotlineID"]);
                            //string SSOKey = Convert.ToString(item["SSO_x0020_Key"]);


                            if (!string.IsNullOrEmpty(DisplayName))
                            {
                                DisplayNameLabel.Text = DisplayName;
                            }
                            else
                            {
                                DisplayNameLabel.Text = userEmail;
                            }

                            /*
                            if (!string.IsNullOrEmpty(ServiceHotlineID))
                            {
                                ServiceHotlineIDLabel.Text = ServiceHotlineID;
                            }
                            else
                            {
                                ServiceHotlineIDLabel.Text = "Contact Isuzu Powertrain";
                            }
                            */

                            if (!string.IsNullOrEmpty(PTWarrantyAccess) && PTWarrantyAccess.Equals("Y"))
                            {
                                PTWarrantySystemHyperLink.Visible = true;
                                PTWarrantySystemHyperLink.NavigateUrl += "?uid=" + Username + "&pwd=" + Password;
                            }

                            if (!string.IsNullOrEmpty(IszjAccess) && IszjAccess.Equals("Y"))
                            {
                                ISZJGlobalServiceHyperLink.Visible = true;
                                ISZJGlobalServiceHyperLink.NavigateUrl += "?lang=en&uid=" + Username.ToUpper() + "&psw=" + Password.ToUpper();
                            }

                            if (!string.IsNullOrEmpty(PTMediaAccess) && PTMediaAccess.Equals("Y"))
                            {
                                PTMediaHyperLink.Visible = true;

                                if (!string.IsNullOrEmpty(UserType) && UserType.Equals("STAFF"))
                                {
                                    PTMediaHyperLink.NavigateUrl += "?admin=Y";
                                }

                            }

                            if (!string.IsNullOrEmpty(UserType) && (UserType.Equals("STAFF") || UserType.Equals("DIST")))
                            {
                                ICSLoginHyperLink.Visible = true;
                                PTStaffHyperLink.Visible = true;
                                NewsletterPanel.Visible = true;
                            }

                        }
                        else if (items != null && items.Count > 1)
                        {
                            ErrorMessageLabel.Visible = true;
                            ErrorMessageLabel.Text = "Error getting User Account Information for user '" + userEmail + "'.";
                        }
                        else
                        {
                            ErrorMessageLabel.Visible = true;
                            ErrorMessageLabel.Text = "No User Account Information defined for user '" + userEmail + "'.";
                        }

                    }

                }

            }

        }

        private void addSSOID(int iItemId, SPList oList)
        {
            string uniqueID = Guid.NewGuid().ToString("N");
            SPListItem _Item = oList.GetItemById(iItemId);
            _Item["SSO_x0020_Key"] = "Aks_" + uniqueID;
            oList.ParentWeb.AllowUnsafeUpdates = true;
            _Item.Update();
            oList.ParentWeb.AllowUnsafeUpdates = false;
        }


        protected void PTWarrantySystemButton_Click(object sender, EventArgs e)
        {

        }

        protected void ISZJGlobalServiceButton_Click(object sender, EventArgs e)
        {

        }

        protected void PTMediaButton_Click(object sender, EventArgs e)
        {

        }


    }
}
