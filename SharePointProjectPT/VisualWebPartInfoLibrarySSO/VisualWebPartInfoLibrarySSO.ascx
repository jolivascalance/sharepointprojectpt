﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %> 
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="VisualWebPartInfoLibrarySSO.ascx.cs" Inherits="SharePointProjectPT.VisualWebPartInfoLibrarySSO.VisualWebPartInfoLibrarySSO" %>




	<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="pingback" href="/xmlrpc.php" />
	<title>Isuzu PowerTrain Resource Center</title>


<link rel='stylesheet' id='js_composer_front-css'  href='/SiteAssets/wp-content/plugins/js_composer/assets/css/js_composer.min.css?ver=5.0.1' type='text/css' media='all' />
<link rel='stylesheet' id='dt-web-fonts-css'  href='//fonts.googleapis.com/css?family=Roboto%3A400%2C500%2C700%7COpen+Sans%3A400%2C600&#038;ver=4.7' type='text/css' media='all' />
<link rel='stylesheet' id='dt-main-css'  href='/SiteAssets/wp-content/themes/dt-the7/css/main.min.css?ver=4.2.1' type='text/css' media='all' />
<style id='dt-main-inline-css' type='text/css'>
body #load {
  display: block;
  height: 100%;
  overflow: hidden;
  position: fixed;
  width: 100%;
  z-index: 9901;
  opacity: 1;
  visibility: visible;
  -webkit-transition: all .35s ease-out;
  transition: all .35s ease-out;
}
body #load.loader-removed {
  opacity: 0;
  visibility: hidden;
}
.load-wrap {
  width: 100%;
  height: 100%;
  background-position: center center;
  background-repeat: no-repeat;
  text-align: center;
}
.load-wrap > svg {
  position: absolute;
  top: 50%;
  left: 50%;
  -ms-transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%);
  transform: translate(-50%,-50%);
}
#load {
  background-color: #23262d;
}
.uil-default rect:not(.bk) {
  fill: #ee2d24;
}
.uil-ring > path {
  fill: #ee2d24;
}
.ring-loader .circle {
  fill: #ee2d24;
}
.ring-loader .moving-circle {
  fill: #ee2d24;
}
.uil-hourglass .glass {
  stroke: #ee2d24;
}
.uil-hourglass .sand {
  fill: #ee2d24;
}
.spinner-loader .load-wrap {
  background-image: url("data:image/svg+xml,%3Csvg width='75px' height='75px' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100' preserveAspectRatio='xMidYMid' class='uil-default'%3E%3Crect x='0' y='0' width='100' height='100' fill='none' class='bk'%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(0 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(30 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.08333333333333333s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(60 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.16666666666666666s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(90 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.25s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(120 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.3333333333333333s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(150 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.4166666666666667s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(180 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(210 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.5833333333333334s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(240 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.6666666666666666s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(270 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.75s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(300 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.8333333333333334s' repeatCount='indefinite'/%3E%3C/rect%3E%3Crect  x='46.5' y='40' width='7' height='20' rx='5' ry='5' fill='%23ee2d24' transform='rotate(330 50 50) translate(0 -30)'%3E  %3Canimate attributeName='opacity' from='1' to='0' dur='1s' begin='0.9166666666666666s' repeatCount='indefinite'/%3E%3C/rect%3E%3C/svg%3E");
}
.ring-loader .load-wrap {
  background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='72' height='72' fill='%23ee2d24'%3E   %3Cpath opacity='.25' d='M16 0 A16 16 0 0 0 16 32 A16 16 0 0 0 16 0 M16 4 A12 12 0 0 1 16 28 A12 12 0 0 1 16 4'/%3E   %3Cpath d='M16 0 A16 16 0 0 1 32 16 L28 16 A12 12 0 0 0 16 4z'%3E     %3CanimateTransform attributeName='transform' type='rotate' from='0 16 16' to='360 16 16' dur='0.8s' repeatCount='indefinite' /%3E   %3C/path%3E %3C/svg%3E");
}
.hourglass-loader .load-wrap {
  background-image: url("data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 32 32' width='72' height='72' fill='%23ee2d24'%3E   %3Cpath transform='translate(2)' d='M0 12 V20 H4 V12z'%3E      %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline'  /%3E   %3C/path%3E   %3Cpath transform='translate(8)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.2' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline'  /%3E   %3C/path%3E   %3Cpath transform='translate(14)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.4' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline' /%3E   %3C/path%3E   %3Cpath transform='translate(20)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.6' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline' /%3E   %3C/path%3E   %3Cpath transform='translate(26)' d='M0 12 V20 H4 V12z'%3E     %3Canimate attributeName='d' values='M0 12 V20 H4 V12z; M0 4 V28 H4 V4z; M0 12 V20 H4 V12z; M0 12 V20 H4 V12z' dur='1.2s' repeatCount='indefinite' begin='0.8' keytimes='0;.2;.5;1' keySplines='0.2 0.2 0.4 0.8;0.2 0.6 0.4 0.8;0.2 0.8 0.4 0.8' calcMode='spline' /%3E   %3C/path%3E %3C/svg%3E");
}


#pageContentTitle
{
    display: none;
}

.js-webpart-titleCell
{
    display: none;
}

</style>
<!--[if lt IE 10]>
<link rel='stylesheet' id='dt-old-ie-css'  href='/SiteAssets/wp-content/themes/dt-the7/css/old-ie.css?ver=4.2.1' type='text/css' media='all' />
<![endif]-->
<link rel='stylesheet' id='dt-awsome-fonts-css'  href='/SiteAssets/wp-content/themes/dt-the7/fonts/FontAwesome/css/font-awesome.min.css?ver=4.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='dt-fontello-css'  href='/SiteAssets/wp-content/themes/dt-the7/fonts/fontello/css/fontello.min.css?ver=4.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='the7pt-static-css'  href='/SiteAssets/wp-content/themes/dt-the7/css/post-type.css?ver=4.2.1' type='text/css' media='all' />
<!--[if lt IE 10]>
<link rel='stylesheet' id='dt-custom-old-ie.less-css'  href='/SiteAssets/wp-content/uploads/wp-less/dt-the7/css/custom-old-ie-038fa273fb.css?ver=4.2.1' type='text/css' media='all' />
<![endif]-->

<link rel='stylesheet' id='dt-custom.less-css'  href='/SiteAssets/wp-content/uploads/wp-less/dt-the7/css/custom-038fa273fb.css?ver=4.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='dt-media.less-css'  href='/SiteAssets/wp-content/uploads/wp-less/dt-the7/css/media-0a98fa5409.css?ver=4.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='the7pt.less-css'  href='/SiteAssets/wp-content/uploads/wp-less/dt-the7/css/post-type-dynamic-0a98fa5409.css?ver=4.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='style-css'  href='/SiteAssets/wp-content/themes/dt-the7/style.css?ver=4.2.1' type='text/css' media='all' />
<link rel='stylesheet' id='bsf-Defaults-css'  href='/SiteAssets/wp-content/uploads/smile_fonts/Defaults/Defaults.css?ver=4.7' type='text/css' media='all' />
<link rel='stylesheet' id='ultimate-style-min-css'  href='/SiteAssets/wp-content/plugins/Ultimate_VC_Addons/assets/min-css/ultimate.min.css?ver=3.16.7' type='text/css' media='all' />
<link rel='stylesheet' id='cp-perfect-scroll-style-css'  href='/SiteAssets/wp-content/plugins/convertplug/modules/slide_in/../../admin/assets/css/perfect-scrollbar.min.css?ver=4.7' type='text/css' media='all' />

<script type='text/javascript' src='/SiteAssets/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
<script type='text/javascript' src='/SiteAssets/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='http://www.google.com/jsapi?ver=4.7'></script>


<script type='text/javascript' src='/SiteAssets/wp-content/themes/dt-the7/js/above-the-fold.min.js?ver=4.2.1'></script>
<script type='text/javascript' src='/SiteAssets/wp-includes/js/jquery/ui/core.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='/SiteAssets/wp-content/plugins/Ultimate_VC_Addons/assets/min-js/ultimate.min.js?ver=3.16.7'></script>
<meta name="generator" content="Powered by LayerSlider 6.1.0 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress." />
<!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->
<!--[if lte IE 9]><link rel="stylesheet" type="text/css" href="/SiteAssets/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css" media="screen"><![endif]--><meta name="generator" content="Powered by Slider Revolution 5.3.1.5 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface." />
<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function (event) {
        var $load = document.getElementById("load");

        var removeLoading = setTimeout(function () {
            $load.className += " loader-removed";
        }, 500);
    });
</script>
<!-- icon -->
<link rel="icon" href="/SiteAssets/wp-content/uploads/sites/4/2016/01/the7-new-fav1.gif" type="image/gif" />
<link rel="shortcut icon" href="/SiteAssets/wp-content/uploads/sites/4/2016/01/the7-new-fav1.gif" type="image/gif" />
<style type="text/css" data-type="vc_shortcodes-custom-css">.vc_custom_1487113855813{padding-top: 40px !important;padding-bottom: 40px !important;}.vc_custom_1479919687423{padding-top: 40px !important;padding-bottom: 70px !important;background-color: #ffffff !important;}.vc_custom_1486416422013{padding-top: 10px !important;padding-bottom: 10px !important;background-color: #000000 !important;}.vc_custom_1484778251238{padding-right: 30px !important;padding-left: 30px !important;}.vc_custom_1484006804396{padding-bottom: 30px !important;}</style><noscript><style type="text/css"> .wpb_animate_when_almost_visible { opacity: 1; }</style></noscript>


<div id="load" class="ring-loader">
	<div class="load-wrap"></div>
</div>
<div id="page">
	<a class="skip-link screen-reader-text" href="#content">Skip to content</a>













    <div id="main" class="sidebar-none" style="padding-top:0px; padding-bottom:0px;" >


		<div class="main-gradient"></div>
		<div class="wf-wrap">
			<div class="wf-container-main">





			<div id="content" class="content" role="main">


                <!--
                <div id="features" class="vc_row wpb_row vc_row-fluid vc_custom_1487113855813">
                    <div class="wpb_column vc_column_container vc_col-sm-12">
                        <div class="vc_column-inner vc_custom_1484778251238">
                            <div class="wpb_wrapper">
                                <div id="ultimate-heading-298258acbeff585ef" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-298258acbeff585ef uvc-4804 " data-hspacer="no_spacer" data-halign="center" style="text-align: center">
                                    <div class="uvc-heading-spacer no_spacer" style="top"></div>
                                    <div class="uvc-main-heading ult-responsive" data-ultimate-target='.uvc-heading.ultimate-heading-298258acbeff585ef h2' data-responsive-json-new='{"font-size":"desktop:56px;mobile_landscape:40px;","line-height":"desktop:66px;mobile_landscape:50px;"}'>
                                        <h2 style="font-weight: normal; margin-bottom: 5px; color: #3b3f4a;">RESOURCE CENTER</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                -->
                <!-- Row Backgrounds -->
                <div class="upb_color" data-bg-override="ex-full" data-bg-color="#f8f8f9" data-fadeout="" data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false" data-custom-vc-row="" data-vc="5.0.1" data-is_old_vc="" data-theme-support="" data-overlay="false" data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size=""></div>
                <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1479919687423 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-md-offset-0 vc_col-sm-offset-0">
                        <div class="vc_column-inner vc_custom_1484006804396">
                            <div class="wpb_wrapper">
                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                        <div class="vc_column-inner ">
                                            <div class="wpb_wrapper">
                                                <div id="ultimate-heading-750558ade3b4d1da7" class="uvc-heading ult-adjust-bottom-margin ultimate-heading-750558ade3b4d1da7 uvc-6585 " data-hspacer="no_spacer" data-halign="left" style="text-align: left">
                                                    <div class="uvc-heading-spacer no_spacer" style="top"></div>
                                                    <div class="uvc-main-heading ult-responsive" data-ultimate-target='.uvc-heading.ultimate-heading-750558ade3b4d1da7 h2' data-responsive-json-new='{"font-size":"desktop:34px;","line-height":"desktop:44px;"}'>
                                                        <h2 style="font-weight: normal; margin-bottom: 8px; color: #3b3f4a;">Welcome to the Isuzu PowerTrain Resource Center</h2>


                                                    </div>
                                                </div>
                                                <div class="wpb_text_column wpb_content_element  vc_custom_1486590808229">
                                                    <div class="wpb_wrapper">

														<h4 style="font-weight: normal; color: #3b3f4a;"><asp:Label ID="DisplayNameLabel" runat="server" Text=""></asp:Label></h4>
														<!--
                                                        <h4 style="font-weight: normal; 8px; color: #3b3f4a;">Isuzu Technical Assistance Center (I-TAC): 1-855-331-3461</h4>
														<h4 style="font-weight: normal; color: #3b3f4a;">I-TAC Access Code: <asp:Label ID="ServiceHotlineIDLabel" runat="server" Text=""></asp:Label></h4>
                                                        -->

                                                        

                                                        <p>This site contains a vast amount of documentation related to ISZAPT Engineering, Sales, Service, Warranty and overall Isuzu information. This Website is designed to enable you to obtain desired information independently and easily. Visit the site frequently for updates on new document releases and upcoming events.
</p>
<p>
For assistance in navigating the site or for descriptions of the site's contents, please click on the <a href="/SiteAssets/PT%20Resource%20Center%20-%20Help%20Guide.pdf" target="_blank">Help</a> option along the top navigation menu bar. If you cannot locate a desired document or require further assistance, click on <a href="/_forms/SPCustomLoginPage/Comments.aspx" target="_blank">Contact Us</a> to e-mail requests or questions to our Website administrator.
</p>


<div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>


<asp:Panel ID= "NewsletterPanel" runat = "server" Visible="false">

<div class="wpb_text_column wpb_content_element  vc_custom_1487701461096">
    <div class="wpb_wrapper">
        <a href="/PowertrainDocuments/001 - PowerTrain News/Isuzu PowerTrain News Vol 1_09-18.pdf" target="_blank"><img src="/SiteAssets/images/PTRC_Newsletter.jpg" width="222" height="79" border="0" /></a>

        <br />
        
        <a href="/PowertrainDocuments/001 - PowerTrain News/Archives" class="btn-shortcode dt-btn-m btn-link accent-btn-color title-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side" target="_blank" id="dt-btn-13"><span>Newsletter Archives</span><i class="fa fa-chevron-right"></i></a>

    </div>
</div>


<div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>
</asp:Panel>





                                                        <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>

                                                        <a href="/PowertrainDocuments" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin" id="A8"><span>Document Library</span><i class="fa fa-chevron-right"></i></a>

                                                        <a href="/Lists/PowertrainAnnouncements" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" target="_blank" id="A9"><span>Announcements</span><i class="fa fa-chevron-right"></i></a>

                                                        <a href="/Lists/PowertrainCalendar" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" target="_blank" id="A2"><span>Calendar</span><i class="fa fa-chevron-right"></i></a>


                                                        <asp:HyperLink id="ICSLoginHyperLink" NavigateUrl="/SitePages/ICSLogin.aspx" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" Target="_blank" runat="server" Visible="false"><span>ICS</span><i class="fa fa-chevron-right"></i></asp:HyperLink>


                                                        <asp:HyperLink id="PTWarrantySystemHyperLink" NavigateUrl="https://warranty.isuzuengines.com/CSOM.aspx" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" Target="_blank" runat="server" Visible="false"><span>Warranty</span><i class="fa fa-chevron-right"></i></asp:HyperLink>


                                                        <asp:HyperLink id="ISZJGlobalServiceHyperLink" NavigateUrl="https://pt-service.isuzu.co.jp/isuzu/login.aspx" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" Target="_blank" runat="server" Visible="false"><span>Global Service</span><i class="fa fa-chevron-right"></i></asp:HyperLink>


                                                        <asp:HyperLink id="PTMediaHyperLink" NavigateUrl="http://ptmedia.isuzuengines.com/index.asp" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" Target="_blank" runat="server" Visible="false"><span>PT Media</span><i class="fa fa-chevron-right"></i></asp:HyperLink>


                                                        <asp:HyperLink id="PTStaffHyperLink" NavigateUrl="/PowertrainDocuments/010%20-%20General%20Isuzu%20Information/Distributor%20Announcements/Isuzu%20Employee%20Contact%20List.pdf" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" Target="_blank" runat="server" Visible="false"><span>PT Staff</span><i class="fa fa-chevron-right"></i></asp:HyperLink>

                                                        <!--
                                                        <asp:HyperLink id="NewsletterHyperLink" NavigateUrl="/PowertrainDocuments/001%20-%20Newsletter" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" Target="_blank" runat="server" Visible="true"><span>Newsletter</span><i class="fa fa-chevron-right"></i></asp:HyperLink>
                                                        -->

                                                        <!--
                                                        <asp:HyperLink id="ContactUsHyperLink" NavigateUrl="/_forms/SPCustomLoginPage/Comments.aspx" class="btn-shortcode dt-btn-m dt-btn default-btn-color default-btn-hover-color default-btn-bg-color default-btn-bg-hover-color ico-right-side anchor-link btn-margin btn-margin-left" Target="_blank" runat="server" Visible="true"><span>Contact Us</span><i class="fa fa-chevron-right"></i></asp:HyperLink>
                                                        -->




                                                        <br />
                                                        <br />
                                                        <br />
                                                        <br />
                                                        <h4 style="font-weight: normal;"><asp:Label ID="ErrorMessageLabel" runat="server" Text="Label" ForeColor="Red" Visible="false"></asp:Label></h4>






                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="vc_empty_space" style="height: 20px"><span class="vc_empty_space_inner"></span></div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row-full-width vc_clearfix"></div>
                <!-- Row Backgrounds -->
                <div class="upb_color" data-bg-override="0" data-bg-color="#ffffff" data-fadeout="" data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false" data-custom-vc-row="" data-vc="5.0.1" data-is_old_vc="" data-theme-support="" data-overlay="false" data-overlay-color="" data-overlay-pattern="" data-overlay-pattern-opacity="" data-overlay-pattern-size="" data-seperator="true" data-seperator-type="triangle_svg_seperator" data-seperator-shape-size="40" data-seperator-svg-height="30" data-seperator-full-width="true" data-seperator-position="top_seperator" data-seperator-background-color="#f8f8f9" data-icon=""></div>
                <div class="vc_row wpb_row vc_row-fluid vc_custom_1486416422013 vc_row-has-fill">
                    <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-lg-offset-0 vc_col-md-offset-0 vc_col-sm-offset-0 vc_col-xs-12">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center; color: white; font-size: large;">We are there for you every step of the way. You matter to
                                            <img style="margin: 0 0 -4px 6px;" src="/SiteAssets/wp-content/uploads/2015/11/Isuzu_big_new.png" width="161" height="29" border="0" /></p>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Row Backgrounds -->
                <div class="upb_bg_img" data-ultimate-bg="url(/SiteAssets/wp-content/uploads/2017/02/iStock_000009345931Large_med.jpg)" data-image-id="id^39254|url^/SiteAssets/wp-content/uploads/2017/02/iStock_000009345931Large_med.jpg|caption^null|alt^null|title^iStock_000009345931Large_med|description^null" data-ultimate-bg-style="vcpb-vz-jquery" data-bg-img-repeat="no-repeat" data-bg-img-size="cover" data-bg-img-position="" data-parallx_sense="30" data-bg-override="ex-full" data-bg_img_attach="scroll" data-upb-overlay-color="rgba(0,0,0,0.6)" data-upb-bg-animation="" data-fadeout="" data-bg-animation="left-animation" data-bg-animation-type="h" data-animation-repeat="repeat" data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30" data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false" data-custom-vc-row="" data-vc="5.0.1" data-is_old_vc="" data-theme-support="" data-overlay="true" data-overlay-color="rgba(0,0,0,0.6)" data-overlay-pattern="" data-overlay-pattern-opacity="0.8" data-overlay-pattern-size="" data-overlay-pattern-attachment="scroll"></div>
                <span class="cp-load-after-post"></span>





            </div><!-- #content -->




			</div><!-- .wf-container -->
		</div><!-- .wf-wrap -->
	</div><!-- #main -->




	<a href="#" class="scroll-top"></a>

</div><!-- #page -->




